using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SampleBlazor.Shared.Models;

namespace SampleBlazor.API.WebAPI.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(Message message)
        {
            var users = new string[] { message.ToUserId, message.FromUserId };
            await Clients.Users(users).SendAsync("ReceiveMessage", message);
            //await Clients.All.SendAsync("ReceiveMessage", message);
        }
    }
}