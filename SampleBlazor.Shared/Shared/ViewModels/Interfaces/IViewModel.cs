﻿using System.ComponentModel;

namespace SampleBlazor.Shared.ViewModels
{
    /// <summary>
    /// Gneric view model interface.
    /// </summary>
    public interface IViewModel : INotifyPropertyChanged
    {
    }
}
