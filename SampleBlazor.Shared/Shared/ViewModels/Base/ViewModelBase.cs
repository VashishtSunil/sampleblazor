﻿using System.ComponentModel;

namespace SampleBlazor.Shared.ViewModels
{
    /// <summary>
    /// Base view model class.
    /// </summary>
    public abstract class ViewModelBase : PropertyChangedBase, IViewModel
    {        
    }
}
