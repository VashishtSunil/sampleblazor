﻿namespace SampleBlazor.Shared.ViewModels
{
    /// <summary>
    /// Base page view model class.
    /// </summary>
    public abstract class PageViewModelBase : ComponentViewModelBase , IPageViewModel
    {
    }
}
