﻿namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// Generic event message interface.
    /// </summary>
    public interface IEventMessage : IMessage , ISerializationType
    {
    }
}
