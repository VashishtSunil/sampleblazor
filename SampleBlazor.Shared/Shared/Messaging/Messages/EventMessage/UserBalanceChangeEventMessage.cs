﻿using MessagePack;
using System.Runtime.Serialization;

namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// User balance changed event message.
    /// </summary>
    [DataContract()]
    [MessagePackObject()]
    public class UserBalanceChangeEventMessage : UserEventMessageBase
    {
    }
}
