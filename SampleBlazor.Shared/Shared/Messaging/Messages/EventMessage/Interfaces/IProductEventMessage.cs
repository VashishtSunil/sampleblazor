﻿namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// Product event message interface.
    /// </summary>
    public interface IProductEventMessage : IEventMessage
    {
    }
}
