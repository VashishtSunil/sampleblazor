﻿using MessagePack;

namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// Entity event message interface.
    /// </summary>
    [Union(0, typeof(EntityChangeEventMessage))]
    public interface IEntityEventMessage : IEventMessage
    {
    }
}
