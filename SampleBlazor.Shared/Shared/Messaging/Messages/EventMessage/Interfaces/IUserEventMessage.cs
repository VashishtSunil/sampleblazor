﻿using MessagePack;

namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// User event message.
    /// </summary>
    [Union(0,typeof(UserBalanceChangeEventMessage))]
    public interface IUserEventMessage : IEventMessage
    {
    }
}
