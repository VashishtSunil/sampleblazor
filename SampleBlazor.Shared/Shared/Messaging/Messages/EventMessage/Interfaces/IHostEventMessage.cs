﻿namespace SampleBlazor.Shared.Messaging
{
    /// <summary>
    /// Host event message interface.
    /// </summary>
    public interface IHostEventMessage : IEventMessage
    {
    }
}
