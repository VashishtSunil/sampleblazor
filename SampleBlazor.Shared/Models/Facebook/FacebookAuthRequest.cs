namespace SampleBlazor.Shared
{
    public class FacebookAuthRequest
    {
        public string AccessToken { get; set; }
    }
}