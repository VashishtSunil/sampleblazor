namespace SampleBlazor.Shared
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }
    }
}