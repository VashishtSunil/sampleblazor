﻿
using Microsoft.AspNetCore.Components;

namespace SampleBlazor.Shared
{
    /// <summary>
    /// Main layout component.
    /// </summary>
    public partial class MainLayout : LayoutComponentBase
    {
        #region PROPERTIES
        [Inject()]
        public AppPageViewModel ViewModel
        {
            get; protected set;
        }
        #endregion

        protected void ToggleNavMenu()
        {
            ViewModel.IsMenuExpanded = !ViewModel.IsMenuExpanded;
        }

        protected void ToggleTheme()
        {
            ViewModel.IsLightTheme = !ViewModel.IsLightTheme;
        }
    }
}
