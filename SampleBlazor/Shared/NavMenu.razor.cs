﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace SampleBlazor.Shared
{
    public partial class NavMenu
    {
        //[Inject]
        //protected Services.IAuthenticationService AuthService { get; set; }

        [Parameter]
        public EventCallback ToggleNavMenuAction { get; set; }

        [Parameter]
        public EventCallback ToggleThemeAction { get; set; }

        private async Task OnClickMenuLogout()
        {
            try
            {
                ///await AuthService.LogoutAsync();
            }
            catch
            {
                throw;
            }
        }
    }
}