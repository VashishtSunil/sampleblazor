﻿using System.Runtime.Serialization;

namespace SampleBlazor.Services
{
    /// <summary>
    /// Access token.
    /// </summary>
    [DataContract()]
    public class AccessToken
    {
        #region PROPERTIES

        /// <summary>
        /// Gets or sets token string.
        /// </summary>
        public string Token
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets refresh token string.
        /// </summary>
        public string RefreshToken
        {
            get; set;
        }

        #endregion
    }
}
