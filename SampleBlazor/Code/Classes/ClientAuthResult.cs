﻿namespace SampleBlazor.Services
{
    public class ClientAuthResult
    {
        #region CONSTRUCTOR
        public ClientAuthResult(AuthResultCode code)
        {
            Result = code;
        }
        #endregion
        
        #region PROPERTIES

        /// <summary>
        /// Gets authentication result code.
        /// </summary>
        public AuthResultCode Result
        {
            get; protected set;
        } 

        #endregion
    }
}
