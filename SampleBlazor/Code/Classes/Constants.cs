﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleBlazor.Code.Classes
{
    /// <summary>
    /// Web manager constants class.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Secure HTTP client name.
        /// </summary>
        public const string SECURE_HTTP_CLIENT_NAME = "SecureHttpClient";
        /// <summary>
        /// Unsecure HTTP client name.
        /// </summary>
        public const string UNSECURE_HTTP_CLIENT_NAME = "UnsecureHttpClient";
        /// <summary>
        /// Default base URL.
        /// </summary>
        public const string DEFAULT_BASE_URL = "https://localhost:44332";
    }
}
