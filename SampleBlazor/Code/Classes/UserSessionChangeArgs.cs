﻿using SampleBlazor.Services;
using System;

namespace SampleBlazor
{
    public class UserSessionChangeArgs : EventArgs
    {
        #region CONSTRUCTOR

        public UserSessionChangeArgs()
        { }

        public UserSessionChangeArgs(UserSessionState state)
        {
            State = state;
        } 

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets current session state.
        /// </summary>
        public UserSessionState State
        {
            get; protected set;
        } 

        #endregion
    }
}
