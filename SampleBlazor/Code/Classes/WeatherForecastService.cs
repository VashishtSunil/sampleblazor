﻿using SampleBlazor.Code.Interfaces;
using SampleBlazor.Pages;
using SampleBlazor.Shared.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SampleBlazor.Code.Classes
{
    public class WeatherForecastService : IWeatherForecastService
    {

        private readonly HttpClient _client;
        private readonly JsonSerializerOptions _options;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public WeatherForecastService(HttpClient client)
        {
            _client = client;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<WeatherForecast>> Get()
        {

            var response = await _client.GetAsync("WeatherForecast");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                throw new ApplicationException(content);
            }
            var products = JsonSerializer.Deserialize<List<WeatherForecast>>(content, _options);
            return products;
           
        }
    }
}
