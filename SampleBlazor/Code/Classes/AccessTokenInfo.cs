﻿using System;

namespace SampleBlazor
{
    /// <summary>
    /// Access token info.
    /// Used to represent locally stored access token information.
    /// </summary>
    public class AccessTokenInfo
    {
        #region CONSTRUCTOR
        public AccessTokenInfo(string accessToken, string refreshToken, DateTime? expireTimeUtc)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            ExpireTimeUTC = expireTimeUtc;
        } 
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets access token.
        /// </summary>
        public string AccessToken
        {
            get; protected set;
        }

        /// <summary>
        /// Gets refresh token.
        /// </summary>
        public string RefreshToken
        {
            get; protected set;
        }

        /// <summary>
        /// Gets access token expiration time.
        /// </summary>
        public DateTime? ExpireTimeUTC
        {
            get; protected set;
        }

        /// <summary>
        /// Gets if token is valid.
        /// Token will be considered valid if AccessToken, RefreshToken and ExpireTimeUTC have a value.
        /// </summary>
        public bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(AccessToken) && !string.IsNullOrWhiteSpace(RefreshToken) && ExpireTimeUTC != null; }
        }

        /// <summary>
        /// Gets if token is expired.
        /// </summary>
        /// <remarks>
        /// The return value will be null if ExpireTimeUTC is not set.
        /// </remarks>
        public bool? IsExpired
        {
            get
            {
                if (ExpireTimeUTC == null)
                    return null;

                return ExpireTimeUTC <= DateTime.UtcNow;
            }
        }

        #endregion
    }
}
