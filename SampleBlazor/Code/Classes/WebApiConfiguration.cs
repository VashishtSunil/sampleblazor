﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace SampleBlazor.Code.Classes
{
    /// <summary>
    /// Application web api configuration.
    /// </summary>
    [DataContract()]
    public class WebApiConfiguration
    {
        #region CONSTRUCTOR

        public WebApiConfiguration()
        { }

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets or sets default API endpoint.
        /// </summary>
        [DefaultValue("https://localhost")]
        [DataMember(EmitDefaultValue = true)]
        public string DefaultWebApiUrl
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets default real time events url.
        /// </summary>
        [DefaultValue("https://localhost/events")]
        [DataMember(EmitDefaultValue = true)]
        public string DefaultRealTimeUrl
        {
            get; set;
        }

        #endregion
    }

}
