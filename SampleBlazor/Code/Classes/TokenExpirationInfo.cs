﻿using System;

namespace SampleBlazor.Services
{
    public class TokenExpirationInfo
    {
        #region CONSTRUCTOR
        public TokenExpirationInfo(int exp, int nbf, int iat)
        {
            if (exp < 0)
                throw new ArgumentOutOfRangeException(nameof(exp), "Invalid tick value specified");

            if (nbf < 0)
                throw new ArgumentOutOfRangeException(nameof(nbf), "Invalid tick value specified");

            if (iat < 0)
                throw new ArgumentOutOfRangeException(nameof(iat), "Invalid tick value specified");

            //parse values
            Expires = EPOCH.AddSeconds(exp);
            Iat = EPOCH.AddSeconds(iat);
            NotBefore = EPOCH.AddSeconds(nbf);
        }
        #endregion

        #region STATIC FIELDS
        private static readonly DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        #endregion

        #region PROPERTIES
        
        /// <summary>
        /// Gets IAT time.
        /// </summary>
        public DateTime Iat
        {
            get; protected set;
        }

        /// <summary>
        /// Gets not before time.
        /// </summary>
        public DateTime NotBefore
        {
            get; protected set;
        }

        /// <summary>
        /// Gets expiration time.
        /// </summary>
        public DateTime Expires
        {
            get; protected set;
        } 

        #endregion

        #region OVERRIDES

        public override string ToString()
        {
            return $"Expires {Expires}, Not Before {NotBefore} , IAT {Iat}";
        } 

        #endregion
    }
}
