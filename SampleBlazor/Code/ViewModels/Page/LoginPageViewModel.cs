﻿using SampleBlazor.Shared.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace SampleBlazor.Pages.ViewModels
{
    public class LoginPageViewModel : PageViewModelBase
    {
        #region FIELDS
        string username = "admin",
            password = "admin";
        bool isActive;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets or sets username.
        /// </summary>
        [Required(ErrorMessage = "Username Required")]
        [StringLength(25, ErrorMessageResourceName = "VE_STRING_LENGTH", ErrorMessageResourceType = typeof(Resources))]
        public string Username
        {
            get { return username; }
            set
            {
                SetProperty(ref username, value);
            }
        }

        /// <summary>
        /// Gets or sets password.
        /// </summary>
        [Required(ErrorMessage = "Password Required")]
        [StringLength(25, ErrorMessage = "Password Required of length")]
        public string Password
        {
            get { return password; }
            set
            {
                SetProperty(ref password, value);
            }
        }

        /// <summary>
        /// Gets or sets if currently active.
        /// </summary>
        public bool IsActive
        {
            get { return isActive; }
            set { SetProperty(ref isActive, value); }
        }

        #endregion
    }
}
