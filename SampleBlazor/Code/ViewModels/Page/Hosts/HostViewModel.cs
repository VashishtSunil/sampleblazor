﻿using SampleBlazor.Shared.ViewModels;
using System.Collections.Generic;

namespace SampleBlazor.Pages.ViewModels
{
    public class HostViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public HostType HostType
        {
            get; set;
        }

        public int Number
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Slots
        {
            get; set;
        }

        public int? HostGroupId
        {
            get; set;
        }

        public int Status
        {
            get; set;
        }

        public ICollection<ActiveSessionViewModel> ActiveSessions
        {
            get; set;
        }
    }
}
