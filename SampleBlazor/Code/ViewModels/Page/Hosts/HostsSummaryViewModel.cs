﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.Pages.ViewModels
{
    public class HostsSummaryViewModel : ViewModelBase
    {
        public int InUse
        {
            get; set;
        }

        public int Free
        {
            get; set;
        }

        public int OutOfOrder
        {
            get; set;
        }
    }
}
