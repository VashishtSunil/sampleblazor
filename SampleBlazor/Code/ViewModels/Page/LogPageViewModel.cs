﻿using SampleBlazor.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleBlazor.Pages.ViewModels
{
    public class LogPageViewModel : PageViewModelBase
    {
        public LogPageViewModel()
        {
            Logs = Enumerable.Range(0, 10).Select(i => new LogViewModel()
            {
                Category = "Generic",
                Module = "Service",
                Version = "2.0.401.0",
                HostName = $"PC-{i}",
                Time = DateTime.Now,
                Message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }).ToList();
        }

        public ICollection<LogViewModel> Logs
        {
            get; set;
        }
    }

    public class LogViewModel : ViewModelBase
    {
        public string Category { get; set; }

        public string Module { get; set; }

        public string Version { get; set; }

        public string HostName { get; set; }

        public DateTime Time { get; set; }

        public string Message { get; set; }

    }
}