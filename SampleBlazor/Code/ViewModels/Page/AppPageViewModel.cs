﻿using SampleBlazor.Shared.ViewModels;
using SampleBlazor.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor
{
    /// <summary>
    /// App page view model.
    /// </summary>
    public class AppPageViewModel : PageViewModelBase
    {
        #region CONSTRUCTOR
        public AppPageViewModel()
        {
        }
        #endregion

        #region FILEDS
        private HashSet<MainMenuNavLinkViewModel> _navLinks;
        private bool isExpanded;
        private bool isLightTheme = true;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets main navigation link models.
        /// </summary>
        public HashSet<MainMenuNavLinkViewModel> NavLinks
        {
            get
            {
                if (_navLinks == null)
                    _navLinks = new HashSet<MainMenuNavLinkViewModel>();
                return _navLinks;
            }
        }

        public bool IsMenuExpanded
        {
            get { return isExpanded; }
            set
            {
                SetProperty(ref isExpanded, value);
            }
        }

        public bool IsLightTheme
        {
            get { return isLightTheme; }
            set { SetProperty(ref isLightTheme, value); }
        }

        #endregion

        #region OVERRIDES

        protected override Task OnInitializeAync(CancellationToken ct = default)
        {
            NavLinks.Clear();

            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./",
                IconClass = "fas fa-home",
                Title = "Home",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./hosts",
                IconClass = "fas fa-desktop",
                Title = "Hosts",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./sales",
                IconClass = "fas fa-chart-line",
                Title = "POS",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./users",
                IconClass = "fas fa-file-invoice",
                Title = "Users",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./waitinglines",
                IconClass = "far fa-hourglass",
                Title = "Waiting lines",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./management",
                IconClass = "fas fa-tasks",
                Title = "Management",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./deployment",
                IconClass = "far fa-window-restore",
                Title = "Deployment",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./monitoring",
                IconClass = "far fa-eye",
                Title = "Monitoring",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./reports",
                IconClass = "fas fa-chart-pie",
                Title = "Reports",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                DefaultRoute = "./log",
                IconClass = "far fa-map",
                Title = "Log",
                RequiredPolicy = null
            });
            NavLinks.Add(new MainMenuNavLinkViewModel()
            {
                //settings link should always be the last item! 
                DefaultRoute = "./settings",
                IconClass = "fas fa-cog",
                Title = "Settings",
                RequiredPolicy = null
            });

            return base.OnInitializeAync(ct);
        }

        #endregion
    }
}
