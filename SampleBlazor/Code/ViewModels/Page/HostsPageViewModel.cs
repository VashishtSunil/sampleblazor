﻿using SampleBlazor.Shared.ViewModels;
using Microsoft.AspNetCore.Components.Web.Virtualization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.Pages.ViewModels
{
    public class HostsPageViewModel : PageViewModelBase
    {
        public HostsPageViewModel()
        {
            Random random = new Random();

            HostsSummary = new HostsSummaryViewModel();
            HostsSummary.InUse = random.Next(100, 1000);
            HostsSummary.Free = random.Next(0, 100);
            HostsSummary.OutOfOrder = random.Next(0, 100);

            HostGroups = Enumerable.Range(0, 5).Select(i => new HostGroupViewModel()
            {
                Id = i,
                Name = $"Group-{i}",
            }).ToList();

            _itemsProviderDelegate = new ItemsProviderDelegate<HostViewModel>(GetResults);
            Hosts = Enumerable.Range(1, 30).Select(i => new HostViewModel()
            {
                Id = i,
                HostType = random.Next(0, 2) == 0 ? HostType.Computer : HostType.Endpoint,
                Number = i,
                Status = random.Next(0, 4)
            }).ToList();

            foreach (var host in Hosts)
            {
                if (host.HostType == HostType.Computer)
                {
                    host.Name = $"PC-{host.Id}";
                    host.Slots = 1;
                }
                else
                {
                    host.Name = $"Endpoint-{host.Id}";
                    host.Slots = random.Next(2, 4);
                }

                host.ActiveSessions = new List<ActiveSessionViewModel>();

                for (int i = 0; i < host.Slots; i++)
                {
                    if (random.Next(0, 2) == 0)
                    {
                        host.ActiveSessions.Add(new ActiveSessionViewModel()
                        {
                            UserId = random.Next(1, 10),
                            HostId = host.Id,
                            SlotNumber = i,
                            Duration = new TimeSpan(random.Next(0, 2), random.Next(0, 59), 0)
                        });
                    }
                }
            }

            UserGroups = new List<UserGroupViewModel>()
            {
                new UserGroupViewModel() { Id = 1,  Name ="Guests" },
                new UserGroupViewModel() { Id = 2,  Name ="Members" },
            };

            Users = Enumerable.Range(1, 10).Select(i => new UserViewModel()
            {
                Id = i,
                IsGuest = random.Next(0, 2) == 0,
                Username = $"User-{i}",
                CreationTime = DateTime.Now,
                Age = random.Next(20, 80)
            }).ToList();
        }

        private MultiSelectionViewModel<HostViewModel> _selectionModel = new MultiSelectionViewModel<HostViewModel>();
        private ItemsProviderDelegate<HostViewModel> _itemsProviderDelegate;

        public ItemsProviderDelegate<HostViewModel> ItemsProvider
        {
            get { return _itemsProviderDelegate; }
        }

        public HostViewModel ActiveHost { get; set; }

        public ICollection<HostGroupViewModel> HostGroups { get; set; }

        public ICollection<HostViewModel> Hosts { get; set; }

        public ICollection<UserGroupViewModel> UserGroups { get; set; }

        public ICollection<UserViewModel> Users { get; set; }

        public ValueTask<ItemsProviderResult<HostViewModel>> GetResults(ItemsProviderRequest request)
        {
            return new ValueTask<ItemsProviderResult<HostViewModel>>
                (new ItemsProviderResult<HostViewModel>(new List<HostViewModel>(Hosts.Skip(request.StartIndex).Take(request.Count)), Hosts.Count));
        }

        public MultiSelectionViewModel<HostViewModel> SelectionModel
        {
            get { return _selectionModel; }
        }

        protected override Task OnInitializeAync(CancellationToken ct = default)
        {
            return base.OnInitializeAync(ct);
        }

        public HostsSummaryViewModel HostsSummary
        {
            get; set;
        }
    }
}