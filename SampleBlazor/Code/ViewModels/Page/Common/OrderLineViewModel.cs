﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.Pages.ViewModels
{
    public class OrderLineViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string ProductName
        {
            get; set;
        }

        public decimal Quantity
        {
            get; set;
        }

        public decimal DeliveredQuantity
        {
            get; set;
        }
    }
}