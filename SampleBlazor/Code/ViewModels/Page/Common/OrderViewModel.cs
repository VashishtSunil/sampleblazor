﻿using SampleBlazor.Shared.ViewModels;
using System.Collections.Generic;

namespace SampleBlazor.Pages.ViewModels
{
    public class OrderViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string UserNote
        {
            get; set;
        }

        public decimal Total
        {
            get; set;
        }

        public ICollection<OrderLineViewModel> OrderLines { get; set; }

    }
}