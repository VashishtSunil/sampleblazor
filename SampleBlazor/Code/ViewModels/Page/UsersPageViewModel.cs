﻿using SampleBlazor.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleBlazor.Pages.ViewModels
{
    public class UsersPageViewModel : PageViewModelBase
    {
        public UsersPageViewModel()
        {
            Random random = new Random();

            MembersSummary = new MembersSummaryViewModel();
            MembersSummary.Total = random.Next(100, 1000);
            MembersSummary.Banned = random.Next(0, 100);
            MembersSummary.Deleted = random.Next(0, 100);

            Users = Enumerable.Range(0, 10).Select(i => new UserViewModel()
            {
                Id = i,
                Username = $"User-{i}",
                CreationTime = DateTime.Now,
                Age = random.Next(20, 80)
            }).ToList();


            Hosts = Enumerable.Range(0, 10).Select(i => new HostViewModel()
            {
                Id = i,
                Name = $"PC-{i}"
            }).ToList();

            UserGroups = Enumerable.Range(0, 10).Select(i => new UserGroupViewModel()
            {
                Id = i,
                Name = $"UserGroup-{i}"
            }).ToList();
        }

        public MembersSummaryViewModel MembersSummary
        {
            get; set;
        }

        public ICollection<UserViewModel> Users
        {
            get; set;
        }

        public ICollection<HostViewModel> Hosts
        {
            get; set;
        }

        public ICollection<UserGroupViewModel> UserGroups
        {
            get; set;
        }
    }

    public class MembersSummaryViewModel : ViewModelBase
    {
        public int Total
        {
            get; set;
        }

        public int Banned
        {
            get; set;
        }

        public int Deleted
        {
            get; set;
        }
    }
}