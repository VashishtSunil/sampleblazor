﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class TimeProductViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public TimeSpan TimeLeft
        {
            get; set;
        }

        public DateTime Purchased
        {
            get; set;
        }

        public bool IsPaid
        {
            get; set;
        }

        public DateTime Expires
        {
            get; set;
        }
    }
}
