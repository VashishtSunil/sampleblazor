﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class SessionViewModel : ViewModelBase
    {
        public string Name
        {
            get; set;
        }

        public DateTime StartTime
        {
            get; set;
        }

        public DateTime? EndTime
        {
            get; set;
        }

        public TimeSpan Duration
        {
            get; set;
        }

        public decimal Charge
        {
            get; set;
        }

        public TimeSpan Prepaid
        {
            get; set;
        }

        public bool IsNew
        {
            get; set;
        }
    }
}