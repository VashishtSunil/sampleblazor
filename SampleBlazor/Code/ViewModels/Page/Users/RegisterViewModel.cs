﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.Pages.ViewModels
{
    public class RegisterViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public int Number
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}