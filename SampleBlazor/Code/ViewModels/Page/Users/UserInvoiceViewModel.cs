﻿using SampleBlazor.Shared.ViewModels;
using System;
using System.Collections.Generic;

namespace SampleBlazor.Pages.ViewModels
{
    public class UserInvoiceViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public int HostId
        {
            get; set;
        }

        public decimal Total
        {
            get; set;
        }

        public int PointsTotal
        {
            get; set;
        }

        public int Award
        {
            get; set;
        }

        public decimal Outstanding
        {
            get; set;
        }

        public bool IsVoided
        {
            get; set;
        }

        public int OperatorId
        {
            get; set;
        }

        public DateTime Date
        {
            get; set;
        }

        public ICollection<InvoiceLineViewModel> InvoiceLines
        {
            get; set;
        }

        public ICollection<InvoicePaymentViewModel> InvoicePayments
        {
            get; set;
        }

        public ICollection<InvoiceRefundViewModel> InvoiceRefunds
        {
            get; set;
        }
    }
}
