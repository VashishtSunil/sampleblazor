﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class AssetTransactionViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int CheckOutOperatorId
        {
            get; set;
        }

        public DateTime CheckOutTime
        {
            get; set;
        }

        public int? CheckInOperatorId
        {
            get; set;
        }

        public DateTime? CheckInTime
        {
            get; set;
        }
    }
}