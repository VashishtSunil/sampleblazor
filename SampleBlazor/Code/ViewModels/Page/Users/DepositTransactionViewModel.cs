﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class DepositTransactionViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public DepositTransactionType Type
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public decimal Balance
        {
            get; set;
        }

        public int PaymentMethodId

        {
            get; set;
        }

        public int OperatorId
        {
            get; set;
        }

        public DateTime CreationTime
        {
            get; set;
        }
    }
}