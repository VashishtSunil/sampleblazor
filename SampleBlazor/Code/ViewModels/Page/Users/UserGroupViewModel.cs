﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.Pages.ViewModels
{
    public class UserGroupViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
