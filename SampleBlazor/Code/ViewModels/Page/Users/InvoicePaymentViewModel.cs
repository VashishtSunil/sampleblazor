﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class InvoicePaymentViewModel : ViewModelBase
    {
        public DateTime Date
        {
            get; set;
        }

        public int OperatorId
        {
            get; set;
        }

        public int RegisterId
        {
            get; set;
        }

        public int PaymentMethodId
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }
    }
}
