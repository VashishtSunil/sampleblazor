﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.Pages.ViewModels
{
    public class ActiveSessionViewModel : ViewModelBase
    {
        public int UserId
        {
            get; set;
        }

        public int HostId
        {
            get; set;
        }

        public int SlotNumber
        {
            get; set;
        }

        public TimeSpan Duration
        {
            get; set;
        }
    }
}
