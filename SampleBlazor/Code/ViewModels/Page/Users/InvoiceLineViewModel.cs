﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.Pages.ViewModels
{
    public class InvoiceLineViewModel : ViewModelBase
    {
        public string ProductName
        {
            get; set;
        }

        public int Quantity
        {
            get; set;
        }

        public decimal UnitPrice
        {
            get; set;
        }

        public int UnitPointsPrice
        {
            get; set;
        }

        public decimal TaxTotal
        {
            get; set;
        }

        public decimal Total
        {
            get; set;
        }
    }
}
