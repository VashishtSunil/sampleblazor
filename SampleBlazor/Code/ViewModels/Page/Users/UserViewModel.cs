﻿using SampleBlazor.Shared.ViewModels;
using System;
using System.ComponentModel.DataAnnotations;

namespace SampleBlazor.Pages.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public bool IsGuest
        {
            get; set;
        }

        [Required]
        [StringLength(30)]
        public string Username
        {
            get; set;
        }

        [StringLength(45)]
        public string FirstName
        {
            get; set;
        }

        [StringLength(45)]
        public string LastName
        {
            get; set;
        }

        [StringLength(254)]
        [EmailNullEmpty]
        public string Email
        {
            get; set;
        }

        [StringLength(20)]
        public string Phone
        {
            get; set;
        }

        [StringLength(20)]
        public string MobilePhone
        {
            get; set;
        }

        [StringLength(255)]
        public string Address
        {
            get; set;
        }

        [StringLength(20)]
        public string PostCode
        {
            get; set;
        }

        [StringLength(45)]
        public string City
        {
            get; set;
        }

        [StringLength(45)]
        public string Country
        {
            get; set;
        }

        [Required]
        public int? UserGroupId
        {
            get; set;
        }

        public Sex Sex
        {
            get; set;
        }

        public decimal Balance
        {
            get; set;
        }

        public TimeSpan Time
        {
            get; set;
        }

        public decimal Deposits
        {
            get; set;
        }

        public int Points
        {
            get; set;
        }

        public decimal Usage
        {
            get; set;
        }

        public int Age
        {
            get; set;
        }

        public DateTime CreationTime
        {
            get; set;
        }

        public ActiveSessionViewModel ActiveSession
        {
            get; set;
        }
    }
}