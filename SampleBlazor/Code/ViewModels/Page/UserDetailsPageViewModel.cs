﻿using SampleBlazor.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleBlazor.Pages.ViewModels
{
    public class UserDetailsPageViewModel : PageViewModelBase
    {
        public UserDetailsPageViewModel()
        {
            Random random = new Random();

            User = new UserViewModel();

            User.Username = "User_" + random.Next(0, 40).ToString();
            User.Points = random.Next(0, 40);
            User.Deposits = random.Next(0, 40);
            User.Balance = random.Next(0, 40);

            Invoices = Enumerable.Range(0, 10).Select(i => new UserInvoiceViewModel()
            {
                Id = i,
                HostId = random.Next(0, 40),
                Total = random.Next(0, 40),
                PointsTotal = random.Next(0, 40),
                Award = random.Next(0, 40),
                Date = DateTime.Now,
                Outstanding = random.Next(0, 40),
                IsVoided = Convert.ToBoolean(random.Next(0, 2))
            }).ToList();

            foreach (var invoice in Invoices)
            {
                invoice.InvoiceLines = Enumerable.Range(0, 5).Select(i => new InvoiceLineViewModel()
                {
                    ProductName = $"Product-{i}",
                    Quantity = random.Next(0, 40),
                    UnitPrice = random.Next(0, 40),
                    UnitPointsPrice = random.Next(0, 40),
                }).ToList();

                invoice.InvoicePayments = Enumerable.Range(0, 5).Select(i => new InvoicePaymentViewModel()
                {
                    Date = DateTime.Now,
                    OperatorId = random.Next(0, 4),
                    RegisterId = random.Next(0, 4),
                    PaymentMethodId = i,
                    Amount = random.Next(0, 40)
                }).ToList();

                invoice.InvoiceRefunds = Enumerable.Range(0, 2).Select(i => new InvoiceRefundViewModel()
                {
                    Date = DateTime.Now,
                    OperatorId = random.Next(0, 4),
                    RegisterId = random.Next(0, 4),
                    RefundMethodId = random.Next(0, 4),
                    Amount = random.Next(0, 40)
                }).ToList();
            }

            TimeProducts = Enumerable.Range(0, 10).Select(i => new TimeProductViewModel()
            {
                Id = i,
                Name = i.ToString(),
                Purchased = DateTime.Now,
                Expires = DateTime.Now,
                IsPaid = Convert.ToBoolean(random.Next(0, 2))
            }).ToList();

            Sessions = Enumerable.Range(0, 10).Select(i => new SessionViewModel()
            {
                Name = i.ToString(),
                StartTime = DateTime.Now,
                IsNew = Convert.ToBoolean(random.Next(0, 2))
            }).ToList();

            DepositTransactions = Enumerable.Range(0, 10).Select(i => new DepositTransactionViewModel()
            {
                Id = i,
                Type = random.Next(0, 2) == 0 ? DepositTransactionType.Deposit : DepositTransactionType.Withdraw,
                Amount = random.Next(0, 40),
                Balance = random.Next(0, 40),
                CreationTime = DateTime.Now,
                PaymentMethodId = random.Next(0, 4)
            }).ToList();

            AssetTransactions = Enumerable.Range(0, 10).Select(i => new AssetTransactionViewModel()
            {
                Id = i,
                Name = i.ToString(),
                CheckOutOperatorId = i,
                CheckOutTime = DateTime.Now
            }).ToList();

            Notes = Enumerable.Range(0, 10).Select(i => new UserNoteViewModel()
            {
                Id = i,
                Text = $"Note-{i}"
            }).ToList();

            Statistics = new UserStatisticsViewModel()
            {
                Logins = 120,
                LoginTime = new TimeSpan(82, 40, 0),
                MemberFor = new TimeSpan(366, 1, 25, 0),
                Games = 15,
                MoneySpentTimeProducts = 130m,
                MoneySpentFixedTime = 20.50m,
                MoneySpentProducts = 19.99m,
                PointsEarned = 250,
                PointsRedeemed = 82
            };
            Statistics.MoneySpentTotal = Statistics.MoneySpentTimeProducts + Statistics.MoneySpentFixedTime + Statistics.MoneySpentProducts;
            Statistics.PointsTotal = Statistics.PointsEarned + Statistics.PointsRedeemed;

            Hosts = Enumerable.Range(0, 10).Select(i => new HostViewModel()
            {
                Id = i,
                Name = $"PC-{i}"
            }).ToList();

            Operators = Enumerable.Range(0, 10).Select(i => new OperatorViewModel()
            {
                Id = i,
                Username = $"Operator-{i}"
            }).ToList();

            Registers = Enumerable.Range(0, 10).Select(i => new RegisterViewModel()
            {
                Id = i,
                Number = i,
                Name = $"Register-{i}"
            }).ToList();

            PaymentMethods = Enumerable.Range(0, 10).Select(i => new PaymentMethodViewModel()
            {
                Id = i,
                Name = $"PaymentMethod-{i}"
            }).ToList();

        }

        public UserViewModel User
        {
            get; set;
        }

        public ICollection<UserInvoiceViewModel> Invoices
        {
            get; set;
        }

        public ICollection<TimeProductViewModel> TimeProducts
        {
            get; set;
        }

        public ICollection<SessionViewModel> Sessions
        {
            get; set;
        }

        public ICollection<DepositTransactionViewModel> DepositTransactions
        {
            get; set;
        }

        public ICollection<AssetTransactionViewModel> AssetTransactions
        {
            get; set;
        }

        public ICollection<UserNoteViewModel> Notes
        {
            get; set;
        }

        public UserStatisticsViewModel Statistics
        {
            get; set;
        }

        public ICollection<HostViewModel> Hosts
        {
            get; set;
        }

        public ICollection<OperatorViewModel> Operators
        {
            get; set;
        }

        public ICollection<RegisterViewModel> Registers
        {
            get; set;
        }

        public ICollection<PaymentMethodViewModel> PaymentMethods
        {
            get; set;
        }
    }

    public class OperatorViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public string Username
        {
            get; set;
        }
    }

    public class UserNoteViewModel : ViewModelBase
    {
        public int Id
        {
            get; set;
        }

        public int Severity
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }
    }

    public class UserStatisticsViewModel : ViewModelBase
    {
        public int Logins
        {
            get; set;
        }

        public TimeSpan LoginTime
        {
            get; set;
        }

        public TimeSpan MemberFor
        {
            get; set;
        }

        public int Games
        {
            get; set;
        }

        public decimal MoneySpentTotal
        {
            get; set;
        }

        public decimal MoneySpentTimeProducts
        {
            get; set;
        }

        public decimal MoneySpentFixedTime
        {
            get; set;
        }

        public decimal MoneySpentProducts
        {
            get; set;
        }

        public int PointsTotal
        {
            get; set;
        }

        public int PointsEarned
        {
            get; set;
        }

        public int PointsRedeemed
        {
            get; set;
        }
    }
}