﻿using SampleBlazor.Shared.ViewModels;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.ViewModels
{
    public class LanguageSelectViewModel : ComponentViewModelBase
    {
        //public LanguageSelectViewModel(ILocalizationService localizationService)
        //{
        //    LocalizationService = localizationService;
        //}

        //public ILocalizationService LocalizationService { get; set; }

        public LanguageSelectViewModel SelectedLanguage { get; set; }

        public HashSet<LanguageSelectionViewModel> Languages { get; set; }

        protected override Task OnInitializeAync(CancellationToken ct = default)
        {
            //Languages = new HashSet<LanguageSelectionViewModel>(LocalizationService.SupportedRegions
            //    .Select(n => new LanguageSelectionViewModel()
            //    {
            //        Name = n.EnglishName,
            //        IsoName = n.TwoLetterISORegionName,
            //    }));

            return base.OnInitializeAync(ct);
        }
    }

    public class LanguageSelectionViewModel
    {
        public string Name { get; set; }
        public string IsoName { get; set; }
    }
}
