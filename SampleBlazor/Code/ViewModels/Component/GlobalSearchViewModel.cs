﻿using SampleBlazor.Shared.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.ViewModels
{
    public class GlobalSearchViewModel : ComponentViewModelBase
    {
        #region FIELDS
        string searchFilter;
        bool isActive;
        IEnumerable<GlobalSearchResultItem> results;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets search results.
        /// </summary>
        public IEnumerable<GlobalSearchResultItem> Results
        {
            get
            {
                if (results == null)
                    results = Enumerable.Empty<GlobalSearchResultItem>();
                return results;
            }
            set
            {
                SetProperty(ref results, value);
            }
        }

        /// <summary>
        /// Gets or sets search filter.
        /// </summary>
        public string SearchFilter
        {
            get { return searchFilter; }
            set { SetProperty(ref searchFilter, value); }
        } 
        
        /// <summary>
        /// Gets if searchg is currently active.
        /// </summary>
        public bool IsActive
        {
            get { return isActive; }
            set { SetProperty(ref isActive, value); }
        }

        #endregion

        #region OVERRIDES
        
        protected override Task OnInitializeAync(CancellationToken ct = default)
        {
            //clear any results
            Results = null;

            //clear current search filter.
            SearchFilter = null;

            return Task.CompletedTask;
        } 

        #endregion
    }
}
