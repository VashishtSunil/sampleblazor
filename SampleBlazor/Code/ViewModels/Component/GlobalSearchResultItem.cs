﻿namespace SampleBlazor.ViewModels
{
    public class GlobalSearchResultItem
    {
        public string Username
        {
            get;set;
        }
    }
}
