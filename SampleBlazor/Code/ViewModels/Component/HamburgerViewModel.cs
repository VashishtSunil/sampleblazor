﻿using SampleBlazor.Shared.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.ViewModels
{
    public class HamburgerViewModel:ComponentViewModelBase
    {
        public HamburgerViewModel()
        {

        }

        #region fields
        private HashSet<HamburgerItems> _hamburgerItems;
        #endregion

        #region Properties
        public HashSet<HamburgerItems> HamburgerItems
        {
            get
            {
                if (_hamburgerItems == null)
                    _hamburgerItems = new HashSet<HamburgerItems>();
                return _hamburgerItems;
            }
        }
        #endregion

        protected override Task OnInitializeAync(CancellationToken ct = default)
        {
            HamburgerItems.Clear();

            HamburgerItems.Add(new ViewModels.HamburgerItems()
            { 
                Icon = "fas fa-expand",
                Name = "Expand",
                Method = "Expand",
                
            });
            HamburgerItems.Add(new ViewModels.HamburgerItems()
            {
                Icon = "far fa-copy",
                Name = "Copy",
                Method = "CopyToClipBoard",
                
            });

            return base.OnInitializeAync(ct);
        }

    }
}
