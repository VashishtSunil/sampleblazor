﻿namespace SampleBlazor.ViewModels
{
    /// <summary>
    /// Represents an nav link model for the main navigation menus.
    /// </summary>
    public class MainMenuNavLinkViewModel : NavLinkViewModelBase
    { 
    }
}
