﻿

using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace SampleBlazor.ViewModels
{
    public class HamburgerItems
    {
        public HamburgerItems()
        {

        }

        public string Name { get; set; }
        public string Icon { get; set; }
        public string Method { get; set; }
    }
}