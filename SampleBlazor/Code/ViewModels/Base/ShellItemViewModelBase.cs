﻿using SampleBlazor.Shared.ViewModels;

namespace SampleBlazor.ViewModels
{
    /// <summary>
    /// Shell item view model base.
    /// </summary>
    public abstract class ShellItemViewModelBase : ViewModelBase
    {
    }
}
