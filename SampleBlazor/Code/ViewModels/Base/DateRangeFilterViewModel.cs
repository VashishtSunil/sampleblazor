﻿using SampleBlazor.Shared.ViewModels;
using System;

namespace SampleBlazor.ViewModels
{
    /// <summary>
    /// Date range filer.
    /// </summary>
    public class DateRangeFilterViewModel : ViewModelBase
    {
        #region FIELDS
        private DateTime? startDate, endDate; 
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets or sets filter start date.
        /// </summary>
        public virtual DateTime? StartDate
        {
            get { return startDate; }
            set { SetProperty(ref startDate, value); }
        }

        /// <summary>
        /// Gets or sets filter end date.
        /// </summary>
        public virtual DateTime? EndDate
        {
            get { return endDate; }
            set { SetProperty(ref endDate, value); }
        } 

        #endregion
    }
}
