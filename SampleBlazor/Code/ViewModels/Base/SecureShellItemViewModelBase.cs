﻿namespace SampleBlazor.ViewModels
{
    public abstract class SecureShellItemViewModelBase : ShellItemViewModelBase
    {
        #region PROPERTIES
        
        /// <summary>
        /// Gets or sets item required policy.
        /// </summary>
        public string RequiredPolicy
        {
            get; set;
        } 

        #endregion
    }
}
