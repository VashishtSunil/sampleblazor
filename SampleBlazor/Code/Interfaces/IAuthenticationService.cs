﻿
using Microsoft.AspNetCore.Components.Authorization;
using SampleBlazor;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.Services
{
    /// <summary>
    /// Authentication service interface.
    /// </summary>
    public interface IAuthenticationService
    {
        #region EVENTS

        /// <summary>
        /// Occurs on session state change.
        /// </summary>
        event EventHandler<UserSessionChangeArgs> SessionStateChanged;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets if user is authenticated.
        /// </summary>
        bool IsAuthenticated { get; }

        /// <summary>
        /// Gets currently authenticated user principal, default principal is returned if user is not logged in.
        /// </summary>
        ClaimsPrincipal User { get; }

        #endregion

        #region FUNCTIONS

        /// <summary>
        /// Initiates user login.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <returns>Client side authentication result.</returns>
        Task<ClientAuthResult> LoginAsync(string username, string password);

        /// <param name="ct">Optional cancellation token.</param>
        ///<inheritdoc cref="LoginAsync(string, string)"/>
        Task<ClientAuthResult> LoginAsync(string username, string password, CancellationToken ct);

        /// <summary>
        /// Gets current authentication state.
        /// </summary>
        /// <returns>Authentication state.</returns>
        Task<AuthenticationState> GetAuthenticationState();

        /// <summary>
        /// Logs out current user.
        /// </summary>
        /// <returns>Associated task.</returns>
        Task LogoutAsync();

        ///<inheritdoc cref="LogoutAsync"/>
        /// <param name="ct">Optional cancellation token.</param>
        Task LogoutAsync(CancellationToken ct);

        /// <summary>
        /// Determines authentication state.
        /// </summary>
        /// <returns>Associated task.</returns>
        /// <remarks>
        /// The method is called upon initialization in order to determine current user authentication state.
        /// </remarks>
        Task DetermineStateAsync();

        #endregion
    }
}
