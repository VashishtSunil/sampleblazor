﻿using SampleBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleBlazor.Code.Interfaces
{
    /// <summary>
    /// Access token based authentication service.
    /// </summary>
    public interface ITokenAuthenticationService : IAuthenticationService
    {
        #region PROPERTIES

        /// <summary>
        /// Gets current access token.
        /// </summary>
        /// <remarks>
        /// This value can be null in case no access token is currently stored in user session.
        /// </remarks>
        string AccessToken { get; }

        /// <summary>
        /// Gets access token.
        /// </summary>
        /// <returns>Current access token.</returns>
        /// <remarks>
        /// The function is responsible of getting locally stored access token and refreshing it in case its expired.
        /// </remarks>
        /// <exception cref="NoTokenException">Thrown in case no stored access token exists.</exception>
        Task<AccessTokenInfo> AccessTokenGetAsync();

        /// <summary>
        /// Requests new access token.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <returns>New access token.</returns>
        /// <exception cref="ArgumentNullException">if <paramref name="username"/> or <paramref name="password"/> values equal to null or empty string.</exception>
        Task<AccessToken> AccessTokenGetAsync(string username, string password);

        /// <summary>
        /// Refreshes existing access token.
        /// </summary>
        /// <param name="token">Existing access token.</param>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>New access token.</returns>
        /// <exception cref="ArgumentNullException">if <paramref name="token"/> or <paramref name="refreshToken"/> values equal to null or empty string.</exception>
        Task<AccessToken> AccessTokenRefreshAsync(string token, string refreshToken);

        #endregion
    }
}
