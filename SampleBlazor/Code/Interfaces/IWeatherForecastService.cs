﻿using SampleBlazor.Shared.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SampleBlazor.Code.Interfaces
{
    public interface IWeatherForecastService 
    {
        public  Task<IEnumerable<WeatherForecast>> Get();
    }
}
