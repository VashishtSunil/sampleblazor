﻿using Microsoft.Extensions.Logging;
using SampleBlazor.Code.Interfaces;
using SampleBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using SampleBlazor.Shared.Messaging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SampleBlazor.Code.Classes;

namespace SampleBlazor.Code.Services
{
    public class EventListenerService : IEventListenerService
    {
        #region CONSTRUCTOR
        public EventListenerService(ITokenAuthenticationService authService, ILogger<EventListenerService> logger, IOptions<WebApiConfiguration> options)
        {
            AuthService = authService ?? throw new ArgumentNullException(nameof(authService));
            AuthService.SessionStateChanged += OnAuthSessionStateChanged;
            Logger = logger;

            //create event url
            EVENT_URL = string.IsNullOrWhiteSpace(options.Value.DefaultRealTimeUrl) ? "https://localhost/events" : options.Value.DefaultRealTimeUrl;

            //create message channel
            _messageChannel = Channel.CreateBounded<IEventMessage>(new BoundedChannelOptions(1000)
            {
                SingleReader = true,
                SingleWriter = true,
                FullMode = BoundedChannelFullMode.Wait
            });
        }
        #endregion

        #region FIELDS
        private HubConnection _connection;
        private readonly SemaphoreSlim _actionLock = new(1, 1);
        private readonly string EVENT_URL = null;
        private readonly Channel<IEventMessage> _messageChannel;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets event message channel.
        /// </summary>
        private Channel<IEventMessage> MessageChannel
        {
            get { return _messageChannel; }
        }

        /// <summary>
        /// Gets authentication service.
        /// </summary>
        private ITokenAuthenticationService AuthService { get; }

        /// <summary>
        /// Gets logger.
        /// </summary>
        private ILogger Logger { get; }

        #endregion

        #region FUNCTIONS

        private async Task CreateConnectionAsync(CancellationToken ct)
        {
            await _actionLock.WaitAsync(ct);
            try
            {
                if (_connection != null)
                {
                    await _connection.StopAsync();
                    await _connection.DisposeAsync();

                    _connection.Reconnecting -= OnConnectionReconnecting;
                    _connection.Reconnected -= OnConnectionReconnected;
                    _connection.Closed -= OnConnectionClosed;

                    _connection = null;
                }

                _connection = new HubConnectionBuilder()
                     //.AddMessagePackProtocol(opt =>
                     //{
                     //    opt.SerializerOptions
                     //    .WithSecurity(MessagePack.MessagePackSecurity.TrustedData);
                     //})
                    .WithAutomaticReconnect()
                    .WithUrl(EVENT_URL, options =>
                    {
                        options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                        options.DefaultTransferFormat = Microsoft.AspNetCore.Connections.TransferFormat.Binary;
                        options.AccessTokenProvider = () => Task.FromResult(AuthService.AccessToken);
                    })
                    .Build();

                _connection.On<IEntityEventMessage>("EntityEvent", OnEntityEvent);

                _connection.Reconnecting += OnConnectionReconnecting;
                _connection.Reconnected += OnConnectionReconnected;
                _connection.Closed += OnConnectionClosed;

                _ = MessageReaderAsync(default);

                await _connection.StartAsync(ct);
            }
            catch (Exception ex)
            {
                Logger.LogCritical(ex, "Error");
            }
            finally
            {
                _actionLock.Release();
            }
        }
       
        private async Task MessageReaderAsync(CancellationToken ct)
        {
            try
            {
                var asyncMessages = MessageChannel.Reader.ReadAllAsync(ct);
                await foreach (var message in asyncMessages)
                {
                    Logger.LogTrace("Message read {message}.", message);
                }
            }
            catch (ChannelClosedException)
            {
                //channel closed
            }
        }

        #endregion

        #region EVENT HANDLERS

        private async void OnAuthSessionStateChanged(object sender, UserSessionChangeArgs e)
        {
            Logger.LogDebug("Authentication state changed to {State}", e.State);

            switch (e.State)
            {
                case UserSessionState.Authenticated:

                    Logger.LogDebug("Authentication state has changed, starting real time connection.");

                    await CreateConnectionAsync(default).ConfigureAwait(false);

                    break;
                case UserSessionState.Authenticating:
                    break;
                default:
                    break;
            }
        }

        private async Task OnEntityEvent(IEntityEventMessage detailedMessage)
        {
            await MessageChannel.Writer.WriteAsync(detailedMessage, default);
        }

        private Task OnConnectionReconnecting(Exception arg)
        {
            return Task.CompletedTask;
        }

        private Task OnConnectionReconnected(string arg)
        {
            return Task.CompletedTask;
        }

        private Task OnConnectionClosed(Exception arg)
        {
            return Task.CompletedTask;
        }

        #endregion
    }
}
