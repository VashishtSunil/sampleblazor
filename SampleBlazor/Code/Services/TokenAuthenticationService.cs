﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using SampleBlazor.Code.Interfaces;
using SampleBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.Code.Services
{
    //[WebApiRoute("auth")]
    public class TokenAuthenticationService : AuthenticationServiceBase, ITokenAuthenticationService
    {
        #region CONSTRUCTOR
        //public TokenAuthenticationService(IHttpClientFactory httpClientFactory,
        //    IJSRuntime jsRuntime,
        //    ILogger<TokenAuthenticationService> logger,
        //    IOptions<WebApiClientOptions> options,
        //    IPayloadSerializerProvider payloadSerializerProvider) :
        //    //injecting http client factory and creating named client is currently only way to use this service as singelton
        //    base(httpClientFactory.CreateClient(Constants.UNSECURE_HTTP_CLIENT_NAME), options, payloadSerializerProvider)
        //{
        //    _jsRuntime = jsRuntime;
        //    Logger = logger;
        //}
        #endregion

        #region CONSTANT
        private const string ACCESS_TOKEN_STORE_KEY_NAME = "accessToken";
        private const string ACCESS_TOKEN_EXPIRATION_STORE_KEY_NAME = "accessTokenExpiration";
        private const string REFRSH_TOKEN_STORE_KEY_NAME = "refreshToken";
        #endregion

        #region READ ONLY FIELDS

        //default json serializer options
        private readonly JsonSerializerOptions JSON_DEFAULT_OPTIONS = new JsonSerializerOptions
        {
            AllowTrailingCommas = true,
            PropertyNameCaseInsensitive = true,
        };

        #endregion

        #region FIELDS

        private readonly SemaphoreSlim LOGIN_OPERATION_LOCK = new(1, 1);
        private readonly SemaphoreSlim ACCESS_TOKEN_STORE_LOCK = new(1, 1);

        private readonly IJSRuntime _jsRuntime;

        #endregion

        #region PROPERTIES

        ///<inheritdoc/>
        public string AccessToken
        {
            get; protected set;
        }

        /// <summary>
        /// Gets current logger instance.
        /// </summary>
        private ILogger<TokenAuthenticationService> Logger { get; }

        #endregion

        #region PRIVATE FUNCTIONS

        private string ConvertName(string name)
        {
            return name switch
            {
                "email" => ClaimTypes.Email,
                "role" => ClaimTypes.Role,
                "unique_name" => ClaimTypes.Name,
                "nameid" => ClaimTypes.NameIdentifier,
                _ => name,
            };
        }

        private ValueTask<ClaimsIdentity> ReadIdentityAsync(string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentNullException(nameof(accessToken));

            //split token segments
            string[] tokenSegments = accessToken.Split('.');

            //check if all segments are present
            if (tokenSegments.Count() < 3)
                throw new ArgumentException("Invalid token. Minimum token segements should be equal to 3.", nameof(tokenSegments));

            //get payload segement
            var payloadSegment = tokenSegments[1];

            payloadSegment = payloadSegment.PadRight(payloadSegment.Length + (payloadSegment.Length * 3) % 4, '=');

            var decodePayload = Convert.FromBase64String(payloadSegment);

            var decodedUtf8Payload = Encoding.UTF8.GetString(decodePayload);

            var result = JsonSerializer.Deserialize<Dictionary<string, dynamic>>(decodedUtf8Payload);

            //generate token claims
            var tokenClaims = result
                .Select(e => new
                {
                    Name = e.Key,
                    Value = e.Value.ValueKind == JsonValueKind.Array ? JsonSerializer.Deserialize<List<string>>(e.Value.ToString()) : new List<string>()
                    {
                        e.Value.ToString()
                    }
                }).ToList();

            //create user claims
            var userClaims = tokenClaims
                .SelectMany(c => (c.Value as List<string>)
                .Select(r => new Claim(ConvertName(c.Name), r)))
                .ToList();

            return new ValueTask<ClaimsIdentity>(new ClaimsIdentity(userClaims, "jwt"));
        }

        private ValueTask<TokenExpirationInfo> ReadExpirationInfoAsync(string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentNullException(nameof(accessToken));

            //split token segments
            string[] tokenSegments = accessToken.Split('.');

            //check if all segments are present
            if (tokenSegments.Count() < 3)
                throw new ArgumentException("Invalid token. Minimum token segements should be equal to 3.", nameof(tokenSegments));

            //get payload segement
            var payloadSegment = tokenSegments[1];

            payloadSegment = payloadSegment.PadRight(payloadSegment.Length + (payloadSegment.Length * 3) % 4, '=');

            var decodePayload = Convert.FromBase64String(payloadSegment);

            var decodedUtf8Payload = Encoding.UTF8.GetString(decodePayload);

            var result = JsonSerializer.Deserialize<Dictionary<string, dynamic>>(decodedUtf8Payload);

            var exp = int.Parse(result["exp"].ToString());
            var nbf = int.Parse(result["nbf"].ToString());
            var iat = int.Parse(result["iat"].ToString());

            return new ValueTask<TokenExpirationInfo>(new TokenExpirationInfo(exp, nbf, iat));
        }

        /// <summary>
        /// Stotes provided access token locally.
        /// </summary>
        /// <param name="token">Access token.</param>
        /// <returns>Associated task.</returns>
        private async Task WriteAccessTokenAsync(AccessToken token, CancellationToken ct = default)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            //read token expiration from the token base64 string
            var tokenExpiration = await ReadExpirationInfoAsync(token.Token);

            await ACCESS_TOKEN_STORE_LOCK.WaitAsync(ct);

            try
            {
                //write access token values to local storage

                await _jsRuntime.InvokeAsync<object>("localStorage.setItem", ACCESS_TOKEN_STORE_KEY_NAME, token.Token);
                await _jsRuntime.InvokeAsync<object>("localStorage.setItem", REFRSH_TOKEN_STORE_KEY_NAME, token.RefreshToken);
                await _jsRuntime.InvokeAsync<object>("localStorage.setItem", ACCESS_TOKEN_EXPIRATION_STORE_KEY_NAME, tokenExpiration.Expires);
            }
            catch
            {
                throw;
            }
            finally
            {
                ACCESS_TOKEN_STORE_LOCK.Release();
            }
        }

        /// <summary>
        /// Reads localy stored access token.
        /// </summary>
        /// <returns>Access token.</returns>
        private async Task<AccessTokenInfo> ReadAccessTokenAsync(CancellationToken ct = default)
        {
            await ACCESS_TOKEN_STORE_LOCK.WaitAsync(ct);

            try
            {
                //read access token values from local storage

                var accessToken = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", ct, ACCESS_TOKEN_STORE_KEY_NAME) != null ?
                    await _jsRuntime.InvokeAsync<string>("localStorage.getItem", ct, ACCESS_TOKEN_STORE_KEY_NAME) :
                    default;

                string refreshToken = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", ct, REFRSH_TOKEN_STORE_KEY_NAME) != null ?
                    await _jsRuntime.InvokeAsync<string>("localStorage.getItem", ct, REFRSH_TOKEN_STORE_KEY_NAME) :
                    default;

                var expirationDateTime = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", ct, ACCESS_TOKEN_EXPIRATION_STORE_KEY_NAME);

                Logger.LogTrace("Read following access token values Access token {accessToken}, Refresh token {refreshToken} , Expiration date {expirationDateTime}", accessToken, refreshToken, expirationDateTime);

                DateTime? tokenExpiration = null;

                //check if expiration date was read from storage and try to convert it
                if (expirationDateTime != null)
                {
                    try
                    {
                        tokenExpiration = DateTime.Parse(expirationDateTime);
                        Logger.LogTrace("Parsed expiration date, value {}", expirationDateTime);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogWarning(ex, "Failed to convert access token expiration date, string value {expirationDateTime}", expirationDateTime);
                    }
                }


                //return access token info
                return new AccessTokenInfo(accessToken, refreshToken, tokenExpiration);
            }
            catch
            {
                throw;
            }
            finally
            {
                ACCESS_TOKEN_STORE_LOCK.Release();
            }
        }

        /// <summary>
        /// Clears locally stored token.
        /// </summary>
        /// <returns>Associated task.</returns>
        private async Task ClearAccessTokenAsync(CancellationToken ct = default)
        {
            await ACCESS_TOKEN_STORE_LOCK.WaitAsync(ct);

            try
            {
                //remove access token values from local storage

                await _jsRuntime.InvokeAsync<object>("localStorage.removeItem", ACCESS_TOKEN_STORE_KEY_NAME);
                await _jsRuntime.InvokeAsync<object>("localStorage.removeItem", REFRSH_TOKEN_STORE_KEY_NAME);
                await _jsRuntime.InvokeAsync<object>("localStorage.removeItem", ACCESS_TOKEN_EXPIRATION_STORE_KEY_NAME);
            }
            catch
            {
                throw;
            }
            finally
            {
                ACCESS_TOKEN_STORE_LOCK.Release();
            }
        }

        private async Task ChangeCurrentTokenAsync(AccessToken accessToken, bool storeAccessToken = true, CancellationToken ct = default)
        {
            if (accessToken == null)
                throw new ArgumentNullException(nameof(accessToken));

            try
            {
                //read identity from the token
                var identity = await ReadIdentityAsync(accessToken.Token);

                //create new principal
                User = new ClaimsPrincipal(identity);

                AccessToken = accessToken.Token;

                //check if we need to store the access token in local storage
                if (storeAccessToken)
                {
                    try
                    {
                        await WriteAccessTokenAsync(accessToken, ct);
                    }
                    catch (Exception ex)
                    {
                        //log failure
                        Logger.LogError(ex, $"Could not store access token.");
                    }
                }
            }
            catch (Exception ex)
            {
                //log failure
                Logger.LogError(ex, $"Could not change current access token.");
            }
        }

        #endregion

        #region OVERRIDES

        public override async Task<ClientAuthResult> LoginAsync(string username, string password, CancellationToken ct)
        {
            // Logger.LogDebug("Executing login of user : {username} , password {password}", username, password);

            await LOGIN_OPERATION_LOCK.WaitAsync(ct);


            try
            {
                //user state is now authenticating
                ChangeSessionState(UserSessionState.Authenticating);

                //get access token
                var accessToken = new AccessToken()
                {
                    Token = "Token",
                    RefreshToken = "RefreshToken"
                };
                //await AccessTokenGetAsync(username, password);

                //change current access token
                //await ChangeCurrentTokenAsync(accessToken, true, ct);

                //user state is now authenticated
                ChangeSessionState(UserSessionState.Authenticated);

                return new ClientAuthResult(AuthResultCode.Sucess);
            }
            catch (Exception ex)
            {
                // Logger.LogError(ex, "Login function failed.");

                //user state is now undetermined
                ChangeSessionState(UserSessionState.Undetermined);

                //return failure result
                return new ClientAuthResult(AuthResultCode.Failure);
            }
            finally
            {
                LOGIN_OPERATION_LOCK.Release();
            }

        }

        public async override Task LogoutAsync(CancellationToken ct)
        {
            //reset current user
            User = null;

            try
            {
                //clear access token information from local store
                await ClearAccessTokenAsync(ct);
            }
            catch (Exception ex)
            {
                //nothing to do here
                Logger.LogError("Failed clearing access token.", ex);
            }

            //notify of session change
            ChangeSessionState(UserSessionState.Undetermined);
        }

        public override Task<AuthenticationState> GetAuthenticationState()
        {
            //here we always return currently stored user as our state
            return Task.FromResult(new AuthenticationState(User));
        }

        public async override Task DetermineStateAsync()
        {
            try
            {
                Logger.LogTrace("Determening authentication state.");

                //make the current state undetermined
                ChangeSessionState(UserSessionState.Undetermined);

                //check if we have a previous token stored in local storage
                var currentTokenInfo = await ReadAccessTokenAsync();

                //log token validity
                Logger.LogTrace("Current token is valid {IsValid}", currentTokenInfo.IsValid);

                //no token present or stored token is invalid then state is undetermined
                if (currentTokenInfo == null || currentTokenInfo.IsValid == false)
                {
                    //notify with event
                    ChangeSessionState(UserSessionState.Undetermined);

                    //no further processing is needed
                    return;
                }

                //create access token instance
                var accesToken = new AccessToken()
                {
                    RefreshToken = currentTokenInfo.RefreshToken,
                    Token = currentTokenInfo.AccessToken,
                };

                //indicates if access token should be stored locally
                bool storeAccessToken = false;

                //check if current token is expired
                if (currentTokenInfo.IsExpired == true)
                {
                    //refresh current access token if expired
                    accesToken = await AccessTokenRefreshAsync(currentTokenInfo.AccessToken, currentTokenInfo.RefreshToken);

                    //store new access token locally
                    storeAccessToken = true;
                }

                //change current token
                await ChangeCurrentTokenAsync(accesToken, storeAccessToken);

                //notify with event
                ChangeSessionState(UserSessionState.Authenticated);
            }
            catch (Exception ex)
            {
                //Logger.LogCritical(ex, "Failed to determine authentication state.");

                //notify with event
                ChangeSessionState(UserSessionState.Undetermined);
            }
        }

        #endregion

        #region PUBLIC FUNCTIONS

        ///<inheritdoc/>
        public async Task<AccessTokenInfo> AccessTokenGetAsync()
        {
            try
            {
                //check if we have token stored
                var accessTokenInfo = await ReadAccessTokenAsync();

                Logger.LogInformation("Stored token is valid {IsValid}.", accessTokenInfo.IsValid);

                //throw exception if no token stored or not all token data is valid
                //if (accessTokenInfo == null || !accessTokenInfo.IsValid) TODO
                //    throw new NoTokenException();

                //check for expiration
                if (accessTokenInfo.IsExpired == true)
                {
                    //refresh token
                    var refreshedAccessToken = await AccessTokenRefreshAsync(accessTokenInfo.AccessToken, accessTokenInfo.RefreshToken);

                    //save the refreshed access token to the storage
                    await WriteAccessTokenAsync(refreshedAccessToken);
                }

                return accessTokenInfo;
            }
            catch
            {
                throw;
            }
        }

        ///<inheritdoc/>
        public async Task<AccessToken> AccessTokenGetAsync(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));
             var  t=new AccessToken()
            {
                Token = "Token",
                RefreshToken = "RefreshToken"
            };

            //create request url TODO
            // var requestUri = CreateRequestUrl("token", $"username={username}&password={password}");

            //return access token
            return  t;
        }

        ///<inheritdoc/>
        public async Task<AccessToken> AccessTokenRefreshAsync(string token, string refreshToken)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));

            if (string.IsNullOrWhiteSpace(refreshToken))
                throw new ArgumentNullException(nameof(refreshToken));

            //create request url
            //var requestUri = CreateRequestUrl("token", $"token={token}&refreshToken={refreshToken}");

            //return refreshed access token
            //return await GetResultAsync<AccessToken>(requestUri);

            return new AccessToken()
            {
                Token = "Token",
                RefreshToken = "RefreshToken"
            };
        }

        #endregion
    }
}
