﻿using Microsoft.AspNetCore.Components.Authorization;
using SampleBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace SampleBlazor.Code.Services
{
    public abstract class AuthenticationServiceBase :  IAuthenticationService
    {
        #region CONSTRUCTOR
        //public AuthenticationServiceBase(HttpClient httpClient, IOptions<WebApiClientOptions> options, IPayloadSerializerProvider payloadSerializerProvider) :
        //    base(httpClient, options, payloadSerializerProvider)
        //{
        //}
        #endregion

        #region EVENTS

        public event EventHandler<UserSessionChangeArgs> SessionStateChanged;

        #endregion

        #region FIELDS
        ClaimsPrincipal user;
        #endregion

        #region PROPERTIES

        ///<inheritdoc/>
        public bool IsAuthenticated
        {
            get { return SessionState == UserSessionState.Authenticated; }
        }

        /// <summary>
        /// Gets or sets user session state.
        /// </summary>
        public UserSessionState SessionState { get; protected set; }

        ///<inheritdoc/>
        public ClaimsPrincipal User
        {
            get
            {
                //always return empty principal to avoid null reference exceptions
                if (user == null)
                    user = new ClaimsPrincipal();
                return user;
            }
            protected set { user = value; }
        }

        #endregion

        #region ABSTRACT FUNCTIONS

        ///<inheritdoc/>
        public virtual Task<ClientAuthResult> LoginAsync(string username, string password)
        {
            return LoginAsync(username, password, default);
        }

        ///<inheritdoc/>
        public abstract Task<ClientAuthResult> LoginAsync(string username, string password, CancellationToken ct);

        ///<inheritdoc/>
        public virtual Task LogoutAsync()
        {
            return LogoutAsync(default);
        }

        ///<inheritdoc/>
        public abstract Task LogoutAsync(CancellationToken ct);

        ///<inheritdoc/>
        public abstract Task<AuthenticationState> GetAuthenticationState();

        ///<inheritdoc/>
        public abstract Task DetermineStateAsync();

        #endregion

        #region PROTECTED VIRTUAL FUNCTIONS

        protected virtual void ChangeSessionState(UserSessionState newState)
        {
            SessionState = newState;
            RaiseSessionStateChanged(new UserSessionChangeArgs(newState));
        }

        protected virtual void RaiseSessionStateChanged()
        {
            RaiseSessionStateChanged(new UserSessionChangeArgs());
        }

        protected virtual void RaiseSessionStateChanged(UserSessionChangeArgs args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            SessionStateChanged?.Invoke(this, args);
        }

        #endregion 
    }
}
