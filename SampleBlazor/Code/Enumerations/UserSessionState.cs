﻿namespace SampleBlazor.Services
{
    /// <summary>
    /// User session state.
    /// </summary>
    public enum UserSessionState
    {
        /// <summary>
        /// Session state is undetermined.
        /// </summary>
        Undetermined=0,

        /// <summary>
        /// Authenticating.
        /// </summary>
        Authenticating=1,

        /// <summary>
        /// Authenticated.
        /// </summary>
        Authenticated=2,
    }
}
