using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SampleBlazor.Code.Classes;
using SampleBlazor.Code.Interfaces;
using SampleBlazor.Code.Services;
using SampleBlazor.Services;
using SampleBlazor.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SampleBlazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {

            //create default web assembly host builder
            var hostBuilder = WebAssemblyHostBuilder.CreateDefault(args);

            //create services variable
            var services = hostBuilder.Services;

           // var builder = WebAssemblyHostBuilder.CreateDefault(args);
            hostBuilder.RootComponents.Add<App>("#app");


            //add token authentication service as singlenton authentication service
            services.AddSingleton<TokenAuthenticationService>();

            //forward interface implementations to the added token authentication service
            services.AddSingleton<ITokenAuthenticationService>(sv => sv.GetRequiredService<TokenAuthenticationService>());
            services.AddSingleton<IAuthenticationService>(sv => sv.GetRequiredService<TokenAuthenticationService>());
            //create services variable
            //var services = hostBuilder.Services;

            //builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            //http client configuration delegate


            #region LOGGING CONFIGURATION
            hostBuilder.Services.AddLogging(logBuilder =>
            {
                //disable some of the log categories
                logBuilder.AddFilter("Microsoft.AspNetCore.Components.RenderTree.Renderer", LogLevel.None);
                logBuilder.AddFilter("Microsoft.AspNetCore.Components.Routing.Router", LogLevel.None);
                logBuilder.AddFilter("Gizmo.Web.Manager.Services.EventListenerService", LogLevel.None);

                //set minimum log level based on configuration
                if (hostBuilder.HostEnvironment.IsDevelopment())
                {
                    //trace all messages in development environment
                    logBuilder.SetMinimumLevel(LogLevel.Trace);
                }
                else
                {
                    //trace warning messages in production environment
                    logBuilder.SetMinimumLevel(LogLevel.Trace);
                }
            });
            #endregion



            //add our localization service
            hostBuilder.Services.AddScoped<IWeatherForecastService,WeatherForecastService>();

            //add event listener service
            services.AddSingleton<EventListenerService>();
            services.AddSingleton<IEventListenerService, EventListenerService>(sv => sv.GetRequiredService<EventListenerService>());


            hostBuilder.Services.AddScoped(sp => new HttpClient { BaseAddress =
                new Uri(Constants.DEFAULT_BASE_URL) });

            #region VIEW MODELS

            //add by type
            foreach (var type in Assembly.GetExecutingAssembly().ExportedTypes)
            {
                //get type info
                var typeInfo = type.GetTypeInfo();

                //must be a class
                if (!typeInfo.IsClass)
                    continue;

                //must be non abstract
                if (typeInfo.IsAbstract)
                    continue;

                //attributes can be checked here for client exclusion e.t.c

                //any type that implement IPageViewModel added as singelton service
                if (typeInfo.GetInterfaces().Any(t => t == typeof(IPageViewModel)))
                    services.AddSingleton(type);

                //any type that implement IComponentViewModel added as singelton service
                if (typeInfo.GetInterfaces().Any(t => t == typeof(IComponentViewModel)))
                    services.AddSingleton(type);
            }

            #endregion
            await hostBuilder.Build().RunAsync();
        }
    }
}
