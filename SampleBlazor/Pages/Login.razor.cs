﻿using SampleBlazor.Components;
//using SampleBlazor.Services;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using SampleBlazor.Code.Interfaces;
using SampleBlazor.Services;

namespace SampleBlazor.Pages
{
    public partial class Login : PageComponentWithModelBase<ViewModels.LoginPageViewModel>
    {
        #region IMPORTS

        [Inject()]
        protected IAuthenticationService AuthService
        {
            get; set;
        }

        //[Inject()]
        //protected ILocalizationService LocalizationService
        //{
        //    get;set;
        //}

        #endregion

        #region FUNCTIONS

        protected async Task OnValidSubmit()
        {
            try
            {
                ViewModel.IsActive = true;
                var result = await AuthService.LoginAsync(ViewModel.Username, ViewModel.Password, default);
            }
            catch
            {
                throw;
            }
            finally
            {
                ViewModel.IsActive = false;
            }
        }

        protected Task OnInvalidSubmit()
        {
            return Task.CompletedTask;
        }

        #endregion
    }
}
